# -*- coding: latin-1 -*-
'''
Created on Feb 13, 2016

@author: Malcolm Bellino
'''
import argparse

# Potential cli format: port-cli [port] tcp|udp|both -v 
# port-cli 22 tcp -v generates: ssh,22,tcp,The Secure Shell (SSH) Protocol,[RFC4251]
# port-cli 22 tcp generates: ssh,22,tcp,
# port-cli 22 udp -v generates: ssh,22,udp,The Secure Shell (SSH) Protocol,[RFC4251]
# port-cli 22 udp generates: ssh,22,udp,

# Output formatting(full verbosity indicated with *):
# Port: 22
# Protocol: TCP
# Service: ssh
# Description: The Secure Shell (SSH) Protocol*
# RFC: RFC4251*

																	# Look up function from ports.csv
def lookup(port,protocol,verbose):
																	# Open ports.csv and create dictionary containing port,protocol:values 
																	# as key vlaue pairs
	dictionary = {} 												# Dictionary to holdport and protocol as key
	with open('ports.csv') as file:
	    for line in file:
	    	key=line.split(",")[0]+","+line.split(",")[1] 			# Split port and protocol and store as key
	    	value=line.split(",",4)									# Split and store line as a list
	    	dictionary[key]=value 									# Store key:value pairs in dictionary
 	
	search="%i,%s"%(port,protocol)									# Create search key from port and protocol
	try:
		if(verbose=='true'):
			print("Port: "+dictionary[search][0])
			print("Protocol: "+dictionary[search][1])
			print("Service: "+dictionary[search][2])
			print("Description: "+dictionary[search][3])
			print("RFC: "+dictionary[search][4].replace("\n","").replace("[","").replace("]",""))
		else:	
			print("Port: "+dictionary[search][0])
			print("Protocol: "+dictionary[search][1])
			print("Service: "+dictionary[search][2])
	except:
		print("Port/protocol combination not found.")

	return

def lookup2(port,protocol,verbose):
	print("lookup2")
	count = 0
	matches= []
	y = []
																	# Open ports.csv and create dictionary containing port,protocol:values 
																	# as key vlaue pairs
	dictionary = {} 												# Dictionary to holdport and protocol as key
	with open('ports.csv') as file:
	    for line in file:
	    	key=line.split(",")[0]+","+line.split(",")[1] 			# Split port and protocol and store as key
	    	value=line.split(",",4)									# Split and store line as a list
	    	dictionary[key]=value 									# Store key:value pairs in dictionary
	    	if(line.split(",")[0] == str(port)):
	    		matches.append(line)

	for x in matches:
		search=matches[count].split(",")[0]+","+matches[count].split(",")[1]
		try:
			if(verbose=='true'):
				print("Port: "+dictionary[search][0])
				print("Protocol: "+dictionary[search][1])
				print("Service: "+dictionary[search][2])
				print("Description: "+dictionary[search][3])
				print("RFC: "+dictionary[search][4].replace("\n","").replace("[","").replace("]",""))
			else:	
				print("Port: "+dictionary[search][0])
				print("Protocol: "+dictionary[search][1])
				print("Service: "+dictionary[search][2])
		except:
			print("Port/protocol combination not found.")
		count+=1
	return




																	# Create parser object
parser = argparse.ArgumentParser(description='This program does port stuff.',
								prog='port-cli',
								epilog='© Malcolm Bellino 2015-2016')

																	# Adds required positional arguments
parser.add_argument('port', help='The port number to look up.')

parser.add_argument('protocol', nargs="?", help='The protocol to look up. Both returns both TCP and UDP results.')

																	# Adds optional verbosity flag
parser.add_argument('-v', '--verbose', help='Returns additional information, such as a description of the protocol and the RFC, if available.', action='store_true')

																	# Add optional version flag
parser.add_argument('--version', help='Returns version information.', action='store_true')

																	# Parse and store arguments
arguments = parser.parse_args()

																	# checking for port and protocol
if(arguments.port):
	port=int(arguments.port)

if(arguments.protocol):
	protocol=arguments.protocol
else:
	protocol=""

verbose=''
if(arguments.verbose):
	verbose='true'
if(protocol != ""):
	lookup(port,protocol,verbose)
else:
	lookup2(port,protocol,verbose)


	


















